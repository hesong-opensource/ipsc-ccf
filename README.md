[![Documentation Status](https://readthedocs.org/projects/hesong-ipsc-ccf/badge/?version=master)](http://hesong-ipsc-ccf.readthedocs.io/en/stable/?badge=master)

# IPSC通用CTI流程

## 开发
使用 IPSC 流程设计器

## 构建 API 文档
API 文档存放在 `docs` 目录，这是一个[Sphinx-Doc]项目，具体请自行阅读该文档机的手册。

## 开发
在 `ipsc-bus-client` 库的基础上进行编程，配合该服务器流程逻辑，实现通用CTI功能。

[Sphinx-Doc]:http://sphinx-doc.com/
